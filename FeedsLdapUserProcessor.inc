<?php

/**
 * @file
 * Creates/updates users from the data given by the LDAP parser.
 */

class FeedsLdapUserProcessor extends FeedsUserProcessor {

  /**
   * Map roles from LDAP to Drupal roles. Is handled by the ldap_authorization module
   *
   * @param String $username
   * @param String $mapping_id
   */
  protected function mapRoles($username, $mapping_id) {
    if ($mapping_id && $username) {
      $user = user_load_by_name($username);
      user_set_authmaps($user, array('authname_ldap_authentication' => $username));
      ldap_authorizations_user_authorizations($user, 'set', $mapping_id);
    }
  }

  /**
   * Add additional user account details
   *
   * @param StdClass $account, User account data
   */
  protected function addAccountDetails(&$account) {
    $timezone = variable_get('date_default_timezone');
    $language = variable_get('language_default');

    if ($timezone) {
      $account->timezone = $timezone;
    }

    if ($language) {
      $account->language = $language->language;
    }
  }


  /**
   * Save a user account.
   */
  protected function entitySave($account) {

    $this->addAccountDetails($account);

    $result = module_invoke_all('feeds_ldap_alter_user_account', $account, $this->id);
    if (is_array($result) && !empty($result)) {
      $account += $result;
    }

    parent::entitySave($account);
    $mapping_id = $this->config[FeedsLdapHelper::CONFIG_MAPPING_ID];
    if ($mapping_id) {
      $this->mapRoles($account->name, $mapping_id);
    }

  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
    FeedsLdapHelper::CONFIG_MAPPING_ID => '',
    FeedsLdapHelper::CONFIG_SYNC_USERS => 0,
    FeedsLdapHelper::CONFIG_CANCEL_METHOD_DELETE => 'user_cancel_delete',
    FeedsLdapHelper::CONFIG_CANCEL_METHOD_SYNC => '',
    ) + parent::configDefaults();
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    global $base_url;

    $form = parent::configForm($form_state);
    $mapping_ids = array('' => t('Do not use mapping')) + FeedsLdapHelper::getMappingIds(true);
    $key = FeedsLdapHelper::CONFIG_MAPPING_ID;
    $form[$key] = array(
      '#type' => 'radios',
      '#title' => t('LDAP Mapping ID'),
      '#description' => t('If you want AD groups to be mapped to Drupal groups you have to select a LDAP mapping ID as defined in the <a href="@ldap_module_url" target="_blank">LDAP module.</a>', array('@ldap_module_url' => $base_url . '/admin/config/people/ldap/authorization')),
      '#default_value' => $this->config[$key],
      '#options' => $mapping_ids,  
    );

    $key = FeedsLdapHelper::CONFIG_CANCEL_METHOD_DELETE;
    $form[$key] = array(
      '#type' => 'radios',
      '#title' => t('Delete method'),
      '#description' => t('Select which method should be used when deleting users using the "Delete items" tab.'),
      '#default_value' => $this->config[$key],
      '#options' => FeedsLdapHelper::getCancelMethods(),       
    );

    $key = FeedsLdapHelper::CONFIG_CANCEL_METHOD_SYNC;
    $form[$key] = array(
      '#type' => 'radios',
      '#title' => t('Sync cancel method'),
      '#description' => t('Select which method should be used when deleting users during sync.'),
      '#default_value' => $this->config[$key],
      '#options' => FeedsLdapHelper::getCancelMethods(),       
    );
      
    $key = FeedsLdapHelper::CONFIG_SYNC_USERS;
    $form[$key] = array(
      '#type' => 'checkbox',
      '#title' => t('Sync LDAP users'),
      '#description' => t('If checked a user that has been removed in the LDAP Server will also be removed in Drupal. This only affects migrated users and work one way: LDAP Server => Drupal.'),
      '#default_value' => $this->config[$key],       
    );

    return $form;
  }

  /**
   * Process the result of the parsing stage.
   *
   * @param FeedsSource $source
   *   Source information about this import.
   * @param FeedsParserResult $parser_result
   *   The result of the parsing stage.
   */
  public function process(FeedsSource $source, FeedsParserResult $parser_result) {
    try {
      parent::process($source, $parser_result);

      if ($this->config[FeedsLdapHelper::CONFIG_SYNC_USERS]
      && $this->config[FeedsLdapHelper::CONFIG_CANCEL_METHOD_SYNC]
      && $source->progressImporting() == FEEDS_BATCH_COMPLETE) {
        $this->syncUsers();
      }
    }
    catch (Exception $e) {
      drupal_set_message($e->getMessage(), 'warning');
    }
  }

  protected function syncUsers() {
    $ldap_users = db_select('authmap', 'a')
    ->fields('a')
    ->condition('module', 'ldap_authentication')
    ->execute();

    $data = array();
    $existing_users = 0;
    $removed_users = array();

    foreach ($ldap_users as $ldap_user) {
      $drupal_user = user_load($ldap_user->uid);
      $ldap_data = ldap_servers_get_user_ldap_data($drupal_user);

      if (!$ldap_data) {
        $removed_users[] = $ldap_user->uid;
      }
      else {
        $existing_users++;  // A better way is to check if we can connect to the server but now we just check if we at least get one valid user. If so, we can safely remove other users
      }
    }

    if ($existing_users && !empty($removed_users)) {
      $cancel_method = $this->config[FeedsLdapHelper::CONFIG_CANCEL_METHOD_DELETE];
      foreach ($removed_users as $user_id) {
        user_cancel(array(), $user_id, 'user_cancel_block');  // @TODO Cancel method as configuration
      }

      db_delete('authmap')->condition('uid', $removed_users, 'IN')->execute();
      db_delete('feeds_item')->condition('entity_id', $removed_users, 'IN')->execute();
    }
  }

  /**
   * Delete multiple user accounts.
   */
  protected function entityDeleteMultiple($uids) {
    $cancel_method = $this->config[FeedsLdapHelper::CONFIG_CANCEL_METHOD_DELETE];
    if ($cancel_method) {
      foreach ($uids as $uid) {
        user_cancel(array(), $uid, $cancel_method);
      }
    }
  }
  
}
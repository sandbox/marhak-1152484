<?php

/**
 * @file
 * Class that executes call to LDAP server and retrieves data
 */

class FeedsLdapFetcherResult extends FeedsFetcherResult {

  protected $server_id;
  protected $search;
  protected $bind_dn;
  protected $bind_pwd;

  public function __construct($server_id, $search, $bind_dn = NULL, $bind_pwd = NULL) {
    $this->server_id = $server_id;
    $this->search = $search;
    $this->bind_dn = $bind_dn;
    $this->bind_pwd = $bind_pwd;
  }

  public function getRaw() {
    $result = array();
    $server = FeedsLdapHelper::getLdapServer($this->server_id);

    if (is_object($server)) {
      $bind_dn = $this->bind_dn ? $this->bind_dn : $server->binddn;
      $bind_pwd = $this->bind_pwd ? $this->bind_pwd : $server->bindpw;
      $base_dn = isset($server->basedn[0]) ? $server->basedn[0] : '';
      $ldap_result = $server->connect();

      if ($base_dn && $ldap_result == LDAP_SUCCESS) {
        $bind_result = $server->bind($bind_dn, $bind_pwd);
        if ($bind_result == LDAP_SUCCESS) {
          $filter = $server->user_attr . '=' . $this->search;
          $result = $server->search($filter, $base_dn);
          $debug = utf8_encode(ob_get_clean());
        }
      }
    }
    
    return $result;
  }
}
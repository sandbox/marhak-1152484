<?php

/**
 * @file
 * Parser that handles the data retrieved by the LDAP fetcher
 */

class FeedsLdapParser extends FeedsParser {

  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $parser_result = new FeedsParserResult();
    $fetcher_result = $fetcher_result->getRaw();
    $items = $this->getItems($fetcher_result);
    $parser_result->items = $items;

    return $parser_result;
  }

  protected function getItems($fetcher_result) {
    $items = array();
    $mapping_keys = array_keys($this->getMappingSources());

    foreach ($fetcher_result as $result) {
      if (is_scalar($result)) {
        continue;
      }

      $item = array();
      foreach ($mapping_keys as $mapping_key) {
        $item[$mapping_key] = isset($result[$mapping_key][0]) ? $result[$mapping_key][0] : '';
      }
      $items[] = module_invoke_all('feeds_ldap_alter_parser_item', $item, $result, $this->id);      
    }

    return $items;
  }

  public function getMappingSources() {
    
    $mapping_sources = parent::getMappingSources() + array(
      'cn' => array(
        'name' => t('Common Name'),
        'description' => t('The first and lastname of an user.'),
      ),
      'ou' => array(
        'name' => t('Organizational unit name'), 
        'description' => t('Organizational unit name')
      ),
      'dc' => array(
        'name' => t('Domain component'),
        'description' => t('Domain component')
      ),
      'samaccountname' => array(
        'name' => t('Account name'),
        'description' => t('The username')
      ),
      'sn' => array(
        'name' => t('Last name'),
        'description' => t('The last name (surename) of the user'),
      ),
      'mail' => array(
        'name' => t('E-mail'),
        'description' => t('E-mail'),
      ),
    );

    return module_invoke_all('feeds_ldap_parser_alter_mapping_sources', $mapping_sources, $this->id);
  }

}
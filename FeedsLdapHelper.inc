<?php

/**
 * @file
 * Helper class with various helper functions making calls to the ldap module
 */

class FeedsLdapHelper {

  const CONFIG_LDAP_SERVER = 'server';
  const CONFIG_MAPPING_ID = 'mapping_id';
  const CONFIG_SYNC_USERS = 'sync_users';
  const CONFIG_CANCEL_METHOD_SYNC = 'cancel_method_sync';
  const CONFIG_CANCEL_METHOD_DELETE = 'cancel_method_delete';

  /**
   * Returns ldap servers defined in the ldap_server module
   *
   * @param boolean $only_enabled
   * @return array
   */
  public static function getLdapServers($only_enabled = FALSE) {
    $servers = array();
    $ldap_servers = ldap_servers_get_servers(null, $only_enabled ? 'enabled' : 'all');

    foreach ($ldap_servers as $server) {
      $servers[$server->sid] = $server->name;
    }

    return $servers;
  }

  /**
   * Returns mappings defined by the ldap_authorization module
   *
   * @param boolean $only_enabled
   * @return array
   */
  public static function getMappings($only_enabled = FALSE) {
    $mappings = array();
    $query = db_select('ldap_authorization', 'la')->fields('la');

    if ($only_enabled) {
      $query = $query->condition('la.status', '1');
    }

    $results = $query->execute();
    foreach ($results as $result) {
      $mappings[] = $result;
    }

    return $mappings;
  }

  /**
   * Returns mapping data for the specified mapping id
   *
   * @param String $mapping_id
   */
  public static function getMapping($mapping_id) {
    return  db_select('ldap_authorization', 'la')->fields('la')->condition('mapping_id', $mapping_id)->execute()->fetchObject();
  }

  /**
   * Returns mapping ids as defined in the ldap_authorization module
   *
   * @param boolean $only_enabled
   */
  public static function getMappingIds($only_enabled = FALSE) {
    $mappings = self::getMappings($only_enabled);
    $mapping_ids = array();

    foreach ($mappings as $mapping) {
      $mapping_ids[$mapping->{self::CONFIG_MAPPING_ID}] = $mapping->{self::CONFIG_MAPPING_ID};
    }

    return $mapping_ids;
  }

  /**
   * Returns a server from the specified server id. Server must be defined by the ldap_server module
   *
   * @param String $server_id
   * @return array
   */
  public static function getLdapServer($server_id) {
    return ldap_servers_get_servers($server_id, 'all', TRUE);
  }

  /**
   * Returns a key value pair of available cancel methods defined in Drupal. Adds a cancel method
   * for not allow cancelation/deletion of users
   *
   * @return array
   */
  public static function getCancelMethods() {
    module_load_include('inc', 'user', 'user.pages');
    $methods = user_cancel_methods();

    $cancel_methods[''] = t('Accounts cannot be canceled/removed.');

    foreach ($methods as $method) {
      $cancel_methods[$method['#return_value']] = $method['#title'];
    }

    return $cancel_methods;
  }
}
<?php

/**
 * @file
 * Class responsible for fetching data using LDAP
 */

module_load_include('inc', 'feeds_ldap', 'FeedsLdapHelper');

class FeedsLdapFetcher extends FeedsFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $server_id = $this->config[FeedsLdapHelper::CONFIG_LDAP_SERVER];
    $source_config = $source->getConfigFor($this);
    $search = isset($source_config['search']) ? $source_config['search'] : '';
            
    return new FeedsLdapFetcherResult($server_id, $search);
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(FeedsLdapHelper::CONFIG_LDAP_SERVER => '');    
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    global $base_url;
    $form = array();
    $key = FeedsLdapHelper::CONFIG_LDAP_SERVER;

    $form[$key] = array(
      '#type' => 'radios',
      '#title' => t('LDAP Server'),
      '#description' => t('Select a LDAP server as defined using the <a href="@ldap_module_url" target="_blank">LDAP module.</a>', array('@ldap_module_url' => $base_url . '/admin/config/people/ldap/servers')),
      '#default_value' => $this->config[$key],
      '#options' => FeedsLdapHelper::getLdapServers()    
    );

    return $form;
  }

  /**
   * Expose source form.
   */
  public function sourceForm($source_config) {
    $form = array();

    $form['search'] = array(
      '#type' => 'textfield',
      '#title' => t('Search'),
      '#description' => t('User search terms'),
      '#default_value' => isset($source_config['search']) ? $source_config['search'] : '*',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );

    return $form;
  }

}